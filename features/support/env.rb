require 'rubygems'
require 'selenium-cucumber'

# Store command line arguments
$browser_type = ENV['BROWSER'] || 'ff'
$platform = ENV['PLATFORM'] || 'desktop'
$os_version = ENV['OS_VERSION']
$device_name = ENV['DEVICE_NAME']
$udid = ENV['UDID']
$app_path = ENV['APP_PATH']

# check for valid parameters
validate_parameters $platform, $browser_type, $app_path

# If platform is android or ios create driver instance for mobile browser
if ($platform == 'android') || ($platform == 'iOS')

  $browser_type = 'Browser' if $browser_type == 'native'

  $device_name, $os_version = get_device_info if $platform == 'android'

  desired_caps = {
    caps: {
      platformName: $platform,
      browserName: $browser_type,
      versionNumber: $os_version,
      deviceName: $device_name,
      udid: $udid,
      app: ".//#{$app_path}"
    }
  }

  begin
    $driver = Appium::Driver.new(desired_caps).start_driver
  rescue Exception => e
    puts e.message
    Process.exit(0)
  end
else # else create driver instance for desktop browser
  begin
    if $browser_type == 'ff'
      options = Selenium::WebDriver::Firefox::Options.new
      options.add_argument('--headless')
      $driver = Selenium::WebDriver.for(:"#{$browser_type}", options: options)
    else
      $driver = Selenium::WebDriver.for(:"#{$browser_type}")
      $driver.manage.window.maximize
    end
  rescue Exception => e
    puts e.message
    Process.exit(0)
  end
end
