Feature: Search feature

  Scenario: Search for "cucumber"
    Given I navigate to "https://duckduckgo.com/"
    And I enter "cucumber" into input field having id "search_form_input_homepage"
    When I click on element having id "search_button_homepage"
    And I wait 30 seconds for element having id "links" to display
    Then I should see page title as "cucumber at DuckDuckGo"
