FROM ruby:2.5.3-alpine

RUN \
  sed -i -e 's/v[[:digit:]]\.[[:digit:]]/edge/g' /etc/apk/repositories \
    && echo 'http://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories \
    && apk upgrade --update-cache --available \
    && apk add --no-cache \
      dbus-x11 \
      dumb-init \
      mesa-gl \
      mesa-dri-swrast \
      ttf-freefont \
      firefox \
      build-base

WORKDIR /app

RUN wget https://github.com/mozilla/geckodriver/releases/download/v0.23.0/geckodriver-v0.23.0-linux64.tar.gz \
  && tar -xvzf geckodriver-v0.23.0-linux64.tar.gz \
  && rm geckodriver-v0.23.0-linux64.tar.gz \
  && chmod +x geckodriver \
  && mkdir -p ./webdrivers \
  && mv geckodriver ./webdrivers

ENV PATH "$PATH:/app/webdrivers"


COPY Gemfile Gemfile.lock ./
RUN bundle install
COPY . .

CMD ["cucumber"]
